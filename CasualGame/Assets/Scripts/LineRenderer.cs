﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRenderer : MonoBehaviour
{
    private LineRenderer tr;
    public float time;

    void Start()
    {
        tr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        tr.time = time;

        if (GameObject.Find("Player").GetComponent<Character>().FirstAnimation == false)
        {
            time = 2.0f;
        }
        else
        {
            if (GameObject.Find("Player").GetComponent<Character>().speed == 1f)
            {
                time = 0.5f;
            }

            if (GameObject.Find("Player").GetComponent<Character>().OnTheLine == true)
            {
                if (time >= 2.0f)
                {
                    time += 0.4f * Time.deltaTime;
                }
                else
                {
                    time = 2.0f;
                }
            }
            else
            {
                if (time >= 0.05f)
                {
                    time -= 0.5f * Time.deltaTime;
                }
                else
                {
                    time = 0.05f;
                }
            }
        }
    }
}
