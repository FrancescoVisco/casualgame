﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseDifficulty : MonoBehaviour
{

    public bool A;
    public bool B;
    public bool C;
    public bool D;
    public bool E;

    void Start()
    {

    }

    void Update()
    {
        if (GameObject.Find("LevelControl").GetComponent<GameController>().npoints < 15)
        {
            A = true;
            B = false;
            C = false;
            D = false;
            E = false;
        }
        else if (GameObject.Find("LevelControl").GetComponent<GameController>().npoints < 55)
        {
            A = false;
            B = true;
            C = false;
            D = false;
            E = false;
        }
        else if (GameObject.Find("LevelControl").GetComponent<GameController>().npoints >= 80)
        {
            A = false;
            B = false;
            C = true;
            D = false;
            E = false;
        }
        else if (GameObject.Find("LevelControl").GetComponent<GameController>().npoints >= 125)
        {
            A = false;
            B = false;
            C = true;
            D = true;
            E = false;
        }
        else if (GameObject.Find("LevelControl").GetComponent<GameController>().npoints >= 125)
        {
            A = false;
            B = false;
            C = true;
            D = false;
            E = true;
        }
    }
}
