﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObstacles : MonoBehaviour
{

    public GameObject[] spawnPrefabs;

    private Transform playerTransform;
    private float spawnY = -10.0f;
    private float spawnLength = 0.4f;
    private int amnOnScreen = 40;
    private float safeZone = 10f;
    private int lastPrefabIndex = 0;
    private int prevIndexx = 0;
    public bool ObliqueUp = true;

    private List<GameObject> activeTiles;

    // Inizializzazione
    void Start()
    {
        activeTiles = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for (int i = 0; i < amnOnScreen; i++)
        {
            SpawnTile();
        }

    }
 
    void Update()
    {
        if (playerTransform)
        {
            if (playerTransform.position.x - safeZone > (spawnY - amnOnScreen * spawnLength))
            {
                SpawnTile();
                DeleteTile();
            }
        }
        else
            return;
    }

    void SpawnTile(int prefabIndex = 0)
    {
        GameObject go;
        int indexx;
        indexx = RandomPrefabIndex();
        spawnY += spawnLength;
        go = Instantiate(spawnPrefabs[indexx]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.right * spawnY;
        activeTiles.Add(go);
        prevIndexx = indexx;
    }

    void DeleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }

    int RandomPrefabIndex()
    {
        int randomIndex = lastPrefabIndex;

        if (GameObject.Find("Player").GetComponent<Character>().GameOn == false)
        {
            randomIndex = Random.Range(0, 0);
        }
        else
        {
            if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().A == true)
            {
                if (lastPrefabIndex == 0)
                {
                    randomIndex = Random.Range(1, 2);
                }

                if (lastPrefabIndex == 1)
                {
                    randomIndex = Random.Range(2, 2);
                }

                if (lastPrefabIndex == 2)
                {
                    randomIndex = Random.Range(3, 4);
                }

                if (lastPrefabIndex == 3)
                {
                    randomIndex = Random.Range(1, 5);
                }

                if (randomIndex == 4)
                {
                    randomIndex = 5;
                }

                if (lastPrefabIndex == 5)
                {
                    randomIndex = Random.Range(1, 2);
                }
            }
            else if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().B == true)
            {
                if (lastPrefabIndex == 0)
                {
                    randomIndex = Random.Range(1, 1);
                }

                if (lastPrefabIndex == 1)
                {
                    randomIndex = Random.Range(2, 2);
                }

                if (lastPrefabIndex == 2)
                {
                    randomIndex = Random.Range(3, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 3)
                {
                    randomIndex = Random.Range(5, 5);
                }

                if (lastPrefabIndex == 4)
                {
                    randomIndex = Random.Range(1, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 5)
                {
                    randomIndex = Random.Range(0, 0);
                }

                if (lastPrefabIndex == 6)
                {
                    randomIndex = Random.Range(0, 0);
                    ObliqueUp = true;

                }
                else if (lastPrefabIndex == 7)
                {
                    randomIndex = Random.Range(0, 0);
                    ObliqueUp = false;
                }

                if (randomIndex == 6 && ObliqueUp == true)
                {
                    randomIndex = 7;
                }

                if (randomIndex == 7 && ObliqueUp == false)
                {
                    randomIndex = 6;
                }
            }
            else if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().C == true || (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().D == true))
            {
                if (lastPrefabIndex == 0)
                {
                    randomIndex = Random.Range(1, 1);
                }

                if (lastPrefabIndex == 1)
                {
                    randomIndex = Random.Range(2, 2);
                }

                if (lastPrefabIndex == 2)
                {
                    randomIndex = Random.Range(3, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 3)
                {
                    randomIndex = Random.Range(4, 4);
                }

                if (lastPrefabIndex == 4)
                {
                    randomIndex = Random.Range(1, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 5)
                {
                    randomIndex = Random.Range(1, 1);
                }

                if (lastPrefabIndex == 6)
                {
                    randomIndex = Random.Range(0, 0);
                    ObliqueUp = true;

                }
                else if (lastPrefabIndex == 7)
                {
                    randomIndex = Random.Range(0, 0);
                    ObliqueUp = false;
                }

                if (randomIndex == 6 && ObliqueUp == true)
                {
                    randomIndex = 7;
                }

                if (randomIndex == 7 && ObliqueUp == false)
                {
                    randomIndex = 6;
                }
            }
            else
            {
                if (lastPrefabIndex == 0)
                {
                    randomIndex = Random.Range(1, 1);
                }

                if (lastPrefabIndex == 1)
                {
                    randomIndex = Random.Range(2, 2);
                }

                if (lastPrefabIndex == 2)
                {
                    randomIndex = Random.Range(3, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 3)
                {
                    randomIndex = Random.Range(4, 5);
                }

                if (lastPrefabIndex == 4)
                {
                    randomIndex = Random.Range(1, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 5)
                {
                    randomIndex = Random.Range(3, spawnPrefabs.Length);
                }

                if (lastPrefabIndex == 6)
                {
                    randomIndex = Random.Range(0, 1);
                    ObliqueUp = true;

                }
                else if (lastPrefabIndex == 7)
                {
                    randomIndex = Random.Range(0, 1);
                    ObliqueUp = false;
                }

                if (randomIndex == 6 && ObliqueUp == true)
                {
                    randomIndex = 7;
                }

                if (randomIndex == 7 && ObliqueUp == false)
                {
                    randomIndex = 6;
                }
            }
        }

        


        lastPrefabIndex = randomIndex;
        //Debug.Log("randomIndex = " + randomIndex);
        return randomIndex;
    }
}