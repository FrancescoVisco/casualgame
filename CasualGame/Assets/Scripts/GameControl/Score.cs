﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{

    public int score;
    public bool reset;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highscoreText;
    public GameObject ScoreObject;

    void Start()
    {
        highscoreText.text = PlayerPrefs.GetInt("Highscore").ToString();
    }

    void Update()
    {
        score = GameObject.Find("LevelControl").GetComponent<GameController>().npoints;
        scoreText.text = score.ToString();

        if (GameObject.Find("Player").GetComponent<Character>().isDead == true)
        {
            if (score > PlayerPrefs.GetInt("Highscore", 0))
            {
                PlayerPrefs.SetInt("Highscore", score);
                highscoreText.text = score.ToString();
            }
        }

        if (GameObject.Find("Player").GetComponent<Character>().GameOn == true)
        {
            ScoreObject.SetActive(true);
        }

        if (reset == true)
        {
            PlayerPrefs.DeleteAll();
            highscoreText.text = "0";
        }
    }
}
