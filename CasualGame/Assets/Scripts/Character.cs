﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    public AudioSource sound_death;
    public AudioSource sound_point;
    public AudioSource sound_grav;
    public AudioSource sound_boost;
    //public AudioSource sound_acc;
    //public AudioSource sound_dec;
    public AudioClip impact;
    public AudioClip point;
    public AudioClip grav;
    public AudioClip boost;
    //public AudioClip acc;
    //public AudioClip dec;

    private Rigidbody2D rb;
    public float speed;
    public int points;
    private float attesa = 0.7f;
    private float attesa1 = 0.4f;
    public float BoostVolume = 0.0f;
    private Scene activeScene;
    
    public bool FirstAnimation = false;
    public bool GameOn = false;
    public bool canTap = true;
    public bool Tap = false;
    public bool SoundOn = true;
    public bool OnTheLine;
    public bool OnTheWall;
    public bool isDead;
    public bool isDeadWall;
    public bool EndAnimation = false;
    public bool alreadyPlayed = false;

    public GameObject DeathWall;
    public GameObject NameText;
    public GameObject RecordText;
    public GameObject PointText;
    public GameObject ps;
    public GameObject psDeathWall;
    public GameObject Flare;


    private TrailRenderer trail;

    public AnimationCurve StartCurve;
    public float LerpTime = 1f;
    public float currentLerpTime;
    public Transform StartPos;
    public Transform EndPos;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        trail = GetComponent<TrailRenderer>();
        activeScene = SceneManager.GetActiveScene();
        Time.timeScale = 1f;
    }

    void Update()
    {
        if (FirstAnimation == true)
        {
            //Attivazione DeathWall
            DeathWall.SetActive(true);

            //Movimento costante sull'asse x
            transform.Translate(Vector2.right * speed * Time.deltaTime);

            //Cambio di gravità
            if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0))
            {
                if (canTap == true)
                {
                    Tap = true;
                }
            }
            else
            {
                if (canTap == true)
                {
                    Tap = false;

                }
            }

                if (Tap == true)
            {
                speed = 1.1f;
                rb.gravityScale = 1.4f;
            }

            //Gestione suono
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (SoundOn == true && isDead == false)
                {
                    sound_grav.PlayOneShot(grav, 0.25f);
                }
            }           

            if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.Mouse0))
            {
                if (SoundOn == true && isDead == false)
                {
                    sound_grav.PlayOneShot(grav, 0.25f);
                }
            }

            if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.Mouse0))
            {
                StartCoroutine(Wait());
            }

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
            {
                StartCoroutine(Wait());
            }

            if(OnTheLine == false)
            {
                if (BoostVolume >= 0.0f)
                {
                    BoostVolume -= 0.50f * Time.deltaTime;
                }
                else
                {
                    BoostVolume = 0.0f;
                }
            }
            else
            {
                if (BoostVolume <= 1.25f)
                {
                    BoostVolume += 0.45f * Time.deltaTime;
                }
                else
                {
                    BoostVolume = 1.35f;
                }
            }

            //Primo tap
            if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0))
            {
                GameOn = true;
                NameText.SetActive(false);
                RecordText.SetActive(false);
                PointText.SetActive(true);
            }

            //Gestione bool canTap e variabile speed
            if (OnTheLine == true)
            {
                GameObject.Find("LevelStructure").GetComponent<Camera>().speed += 0.01f;
                speed = 1.625f;
            }

            if (OnTheWall == true)
            {
                if (GameOn == true)
                {
                    speed = 0.7f;
                }
                else
                {
                    speed = 1f;
                }
            }

            if(OnTheLine == false && OnTheWall == false && Tap == false)
            {
                speed = 0.7f;
            }
        }
        else
        {
            currentLerpTime += Time.deltaTime;

            if(currentLerpTime > LerpTime)
            {
                currentLerpTime = LerpTime;
            }

            float t = StartCurve.Evaluate(currentLerpTime); 
            transform.position = Vector3.Lerp(StartPos.position, EndPos.position, t);
        }

        if(isDead == true)
        {
            StartCoroutine(Death());
        }


        if(EndAnimation == true)
        {
            SceneManager.LoadScene(activeScene.name);
        }

        if (canTap == false)
        {
            if(Tap == true)
            {
                rb.gravityScale = 1.7f;              
            }else
            {
                rb.gravityScale = -1.7f;
            }
        }        
    }



    //Iniziallizzazione bool OntheLine
    void FixedUpdate()
    {
        sound_boost.volume = BoostVolume;
        OnTheLine = false;

        if (Tap == false)
        {
            rb.gravityScale = -1.4f;
        }
    }



    //Collisioni

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            OnTheWall = true;
        }

        if (collision.gameObject.tag == "DeathLine")
        {
            //Debug.Log("Death");    
            sound_death.PlayOneShot(impact, 1.0f);
            isDead = true;
            canTap = false;
        }

        if (collision.gameObject.tag == "DeathWall")
        {
            //Debug.Log("Death");    
            sound_death.PlayOneShot(impact, 1.0f);
            isDead = true;
            isDeadWall = true;
            canTap = false;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "NormalLine")
        {
            //Debug.Log("OnTheLine");                
            OnTheLine = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            //Debug.Log("OnTheWall");
            OnTheWall = false;
        }

        if (collision.gameObject.tag == "NormalLine")
        {
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PointLine")
        {
            //Debug.Log("Point");
            sound_point.PlayOneShot(point, 0.8f);
            speed = 2.0f;
            points += 1;           
        }

        if(collision.gameObject.tag == "EndPos")
        {
            FirstAnimation = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
    }

    IEnumerator Wait()
    {
        //Debug.Log("Coroutine");
        SoundOn = false;
        yield return new WaitForSeconds(attesa1);
        SoundOn = true;
    }

    IEnumerator Death()
    {
        this.GetComponent<SpriteRenderer>().enabled = false;
        BoostVolume -= 0.3f * Time.deltaTime;
        GameObject.Find("RainFallParticleSystem").GetComponent<ScrollerEffect>().ScrollSpeed = 1.3f;
        Time.timeScale = 0.3f;
        if (isDeadWall == false)
        {
            ps.SetActive(true);
        }
        else
        {
            psDeathWall.SetActive(true);
        }    
        Flare.SetActive(false);
        speed = 0;
        yield return new WaitForSeconds(attesa);
        EndAnimation = true;
    }
}
