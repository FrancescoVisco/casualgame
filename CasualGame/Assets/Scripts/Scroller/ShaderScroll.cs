﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderScroll : MonoBehaviour
{
    public float speed;
    public Material material;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        material.SetFloat("Vector1_AE5FDB7D", speed);

        if (GameObject.Find("Player").GetComponent<Character>().FirstAnimation == false)
        {
            speed = 0.8f;
        }
        else
        {
            if (GameObject.Find("Player").GetComponent<Character>().speed == 1f && GameObject.Find("Player").GetComponent<Character>().GameOn == false)
            {
                speed = 0.79f;
            }

            if (GameObject.Find("Player").GetComponent<Character>().OnTheLine == true)
            {
                speed = 0.81f;
            }
            else
            {
                speed = 0.79f;
            }
        }
    }
}


