using UnityEngine;
using System.Collections;

//#pragma strict
//[RequireComponent(typeof(LineRenderer))]

public class ScrollerEffect : MonoBehaviour
{
    /*
    public float scrollSpeed = 1.0f;
    public float MainoffsetX = 0.0f;
    public float MainoffsetY = 0.0f;

    public bool UseCustomTex = false;
    public string CustomTexName = "";

    void Update()
    {
        float offset = Time.time * scrollSpeed;
        if (UseCustomTex) {
            GetComponent<Renderer>().material.SetTextureOffset(CustomTexName, new Vector2(MainoffsetX * offset, MainoffsetY * offset));
        }
        else {
            GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(MainoffsetX * offset, MainoffsetY * offset));

        }
    }*/

    private ParticleSystem ps;
    public float ScrollSpeed;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        var main = ps.main;
        main.simulationSpeed = ScrollSpeed;

        if (GameObject.Find("Player").GetComponent<Character>().FirstAnimation == false)
        {
            ScrollSpeed = 4.0f;
        }
        else
        {
            if (GameObject.Find("Player").GetComponent<Character>().speed == 1f)
            {
                ScrollSpeed = 0.8f;
            }

            if (GameObject.Find("Player").GetComponent<Character>().OnTheLine == true)
            {
                if(ScrollSpeed <= 3.0f)
                {
                    ScrollSpeed += 2.0f * Time.deltaTime;
                }
                else
                {
                    ScrollSpeed = 3.0f;
                }
                    
            }
            else if(GameObject.Find("Player").GetComponent<Character>().OnTheWall == true)
            {
                if (ScrollSpeed >= 0.6f)
                {
                    ScrollSpeed -= 1.0f * Time.deltaTime;
                }
                else
                {
                    ScrollSpeed = 0.6f;
                }
            }
            else
            {
                if (ScrollSpeed >= 0.6f)
                {
                    ScrollSpeed -= 1.0f * Time.deltaTime;
                }
                else
                {
                    ScrollSpeed = 0.6f;
                }
            }
        }
    }
}
