﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollerBackground : MonoBehaviour
{

    private ParticleSystem ps;
    public float ScrollSpeed;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        var main = ps.main;
        main.simulationSpeed = ScrollSpeed;

        if (GameObject.Find("Player").GetComponent<Character>().FirstAnimation == false)
        {
            ScrollSpeed = 5.0f;
        }
        else
        {
            if (GameObject.Find("Player").GetComponent<Character>().speed == 1f)
            {
                ScrollSpeed = 0.2f;
            }

            if (GameObject.Find("Player").GetComponent<Character>().OnTheLine == true)
            {
                if (ScrollSpeed <= 4.0f)
                {
                    ScrollSpeed += 0.9f * Time.deltaTime;
                }
                else
                {
                    ScrollSpeed = 4.0f;
                }

            }
            else if (GameObject.Find("Player").GetComponent<Character>().OnTheWall == true)
            {
                if (ScrollSpeed >= 0.3f)
                {
                    ScrollSpeed -= 0.5f * Time.deltaTime;
                }
                else
                {
                    ScrollSpeed = 0.3f;
                }
            }
            else
            {
                if (ScrollSpeed >= 0.3f)
                {
                    ScrollSpeed -= 0.5f * Time.deltaTime;
                }
                else
                {
                    ScrollSpeed = 0.3f;
                }
            }
        }
    }
}