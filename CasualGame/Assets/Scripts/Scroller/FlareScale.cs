﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareScale : MonoBehaviour
{
    public float x;
    public float y;

    void Start()
    {

    }

    void Update()
    {

        //Scale Flare
        y = x;
        transform.localScale = new Vector3(x, y, 1);

        if (GameObject.Find("Player").GetComponent<Character>().FirstAnimation == false)
        {
            x = 4.3f;
        }
        else
        {
            if (GameObject.Find("Player").GetComponent<Character>().speed == 1f)
            {
                x = 4.0f;
            }

            if (GameObject.Find("Player").GetComponent<Character>().OnTheLine == true)
            {
                if (x <= 8.0f)
                {
                    x += 3.0f * Time.deltaTime;
                }
                else
                {
                    x = 8.0f;
                }
            }
            else if(GameObject.Find("Player").GetComponent<Character>().OnTheWall == true)
            {
                if (x >= 1.0f)
                {
                    x -= 1.7f * Time.deltaTime;
                }
                else
                {
                    x = 1.0f;
                }
            }
            else
            {
                if (x >= 1.0f)
                {
                    x -= 1.2f * Time.deltaTime;
                }
                else
                {
                    x = 1.0f;
                }
            }
        }
    }
}