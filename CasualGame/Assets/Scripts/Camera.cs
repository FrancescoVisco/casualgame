﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public float speed;

    void Start()
    {
    }

    void Update()
    {
        //Movimento costante sull'asse x
        transform.Translate(Vector2.right * speed * Time.deltaTime); transform.Translate(Vector2.right * speed * Time.deltaTime);

        //Gestione variabile speed
        if(GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().A == true)
        {
            speed = 0.5f;
        }

        if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().B == true)
        {
            speed = 0.53f;
        }

        if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().C == true)
        {
            speed = 0.58f;
        }

        if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().D == true)
        {
            speed = 0.64f;
        }

        if (GameObject.Find("LevelControl").GetComponent<IncreaseDifficulty>().E == true)
        {
            speed = 0.7f;
        }
    }
}
