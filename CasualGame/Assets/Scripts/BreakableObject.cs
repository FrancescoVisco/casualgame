﻿using System.Collections;
using UnityEngine;

public class BreakableObject : MonoBehaviour
{
    public GameObject ps;

    void Start()
    {
    }

    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Collisione Player e attivazione ParticleSystem
        if (collision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
            ps.SetActive(true);
        }
    }  

  
}
